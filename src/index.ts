import 'zone.js/dist/zone-node';
import 'reflect-metadata';
import { enableProdMode } from '@angular/core';
import { ngExpressEngine } from '@nguniversal/express-engine';
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';
import { Express } from 'express'
import { join } from 'path';
import { NgUniversalConfig, WinterPlugin, Environment } from 'winter-core'

export class NgExpressPlugin implements WinterPlugin {
  private AppServerModuleNgFactory
  private LAZY_MODULE_MAP
  private config:NgUniversalConfig

  private constructor(
    angularModules?:any
  ){
    const { AppServerModuleNgFactory, LAZY_MODULE_MAP } = angularModules
    this.AppServerModuleNgFactory = AppServerModuleNgFactory
    this.LAZY_MODULE_MAP = LAZY_MODULE_MAP
  }

  private static _instance: NgExpressPlugin
  static getInstance(angularModules:any): NgExpressPlugin {
    if (!NgExpressPlugin._instance) {
      NgExpressPlugin._instance = new NgExpressPlugin(angularModules)
    }
    return this._instance
  }

  apply(app: Express, environment:Environment, express) {
    if(environment){
      this.config = environment.ngUniversal
    }

    const { dist, browser } = this.config

    enableProdMode();

    const DIST_FOLDER = join(process.cwd(), dist);
    const BROWSER_FOLDER = join(DIST_FOLDER, browser)
    
    app.engine('html', ngExpressEngine({
      bootstrap: this.AppServerModuleNgFactory,
      providers: [
        provideModuleMap(this.LAZY_MODULE_MAP)
      ]
    }));

    app.set('view engine', 'html');
    app.set('views', BROWSER_FOLDER);

    // Server static files from /browser
    app.get('*.*', express.static(BROWSER_FOLDER, {
      maxAge: '1y'
    }));

    // All regular routes use the Universal engine
    app.get('*', (req, res) => {
      res.render('index', { req });
    });
  }
 
}